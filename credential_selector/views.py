import os
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages

from .forms import WifiForm

static_ip_config = [
    'interface wlan0\n',
    '    static ip_address=192.168.4.1/24\n',
    '    nohook wpa_supplicant\n'
]

first_request = True

def credentials(request):
    # if this is a POST request we need to process the form data
    global first_request

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = WifiForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            ssid = form.cleaned_data['ssid']
            passw = form.cleaned_data['password']
            update_credentials(ssid, passw)

            messages.add_message(
                request, messages.INFO, 'The credentials have been saved correctly')

            if first_request:
                configure_system_to_auto_connect()
                first_request = False

    return render(request, 'credential_selector/credentials.html', {'form': WifiForm()})

def update_credentials(ssid, passw):
    with open('/etc/wpa_supplicant/wpa_supplicant.conf', 'a') as f:
        f.write('\nnetwork={\n')
        f.write('\tssid="{}"\n'.format(ssid))
        f.write('\tpsk="{}"\n'.format(passw))
        f.write('\tkey_mgmt=WPA-PSK\n')
        f.write('}\n')

def configure_system_to_auto_connect():
    os.system('sudo systemctl disable hostapd.service')
    os.system('sudo systemctl disable dnsmasq.service')

    with open('/etc/dhcpcd.conf', 'r') as f:
        lines = f.readlines()

    with open('/etc/dhcpcd.conf', 'w') as f:
        for line in lines:
            if line not in static_ip_config:
                f.write(line)