from django.apps import AppConfig


class CredentialSelectorConfig(AppConfig):
    name = 'credential_selector'
