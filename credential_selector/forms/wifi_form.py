from django import forms
import subprocess

class WifiForm(forms.Form):
    ssid = forms.ChoiceField(label='SSID', widget=forms.Select(attrs={'class': 'fadeIn second'}))
    password = forms.CharField(min_length=8, widget=forms.PasswordInput(attrs={'class': 'fadeIn third'}))

    def __init__(self, *args, **kwargs):
        super(WifiForm, self).__init__(*args, **kwargs)
        self.fields['ssid'].choices = self.get_detected_ssids()

    def clean_ssid_list(self, ap_ssid):
        ap_ssid = ap_ssid.replace(" ", "")
        ap_ssid = ap_ssid.replace("ESSID:", "")
        ap_ssid = ap_ssid.replace('"', '')

        return ap_ssid
    
    def get_detected_ssids(self):
        scanprocess = subprocess.Popen("sudo iwlist scan | grep SSID", shell=True, stdout=subprocess.PIPE)
        scanprocess_return = scanprocess.stdout.read() 
        ap_list = scanprocess_return.decode().split("\n")
        del ap_list[-1]
        ap_list = list(map(self.clean_ssid_list, ap_list))
        ap_list = [(ssid, ssid) for ssid in ap_list if ssid != ""]
        return ap_list