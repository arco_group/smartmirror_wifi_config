/* global Module */

/* Magic Mirror
 * Module: QRCode
 *
 * By Evghenii Marinescu https://github.com/MarinescuEvghenii/
 * MIT Licensed.
 */

'use strict';

Module.register("MMM-QRCode", {

	defaults: {
		text       : 'http://192.168.4.1:8000/',
		colorDark  : "#000",
		colorLight : "#fff",
		imageSize  : 125,
		showRaw    : false
	},

	getStyles: function() {
		return ["MMM-QRCode.css"];
	},

	getScripts: function() {
		return ["qrcode.min.js"];
	},


	start: function() {
		this.config = Object.assign({}, this.defaults, this.config);
		Log.log("Starting module: " + this.name);
	},

	getDom: function() {
		const wrapperEl = document.createElement("div");
	  	
		var header = document.createElement('header');
	  	header.innerHTML = 'Step 2: Configure smartmirror with your wifi';
	  	wrapperEl.appendChild(header);
		
		wrapperEl.classList.add('qrcode');

		const qrcodeEl  = document.createElement("div");
		qrcodeEl.classList.add('qr');
		new QRCode(qrcodeEl, {
			text: this.config.text,
			width: this.config.imageSize,
			height: this.config.imageSize,
			colorDark : this.config.colorDark,
			colorLight : this.config.colorLight,
			correctLevel : QRCode.CorrectLevel.H
		});

		const back = document.createElement("div");
		back.style.width = '145px';
		back.style.height = '145px';
		
		back.style.backgroundColor = '#fff';

		const imageEl  = document.createElement("div");
		imageEl.classList.add('qrcode__image');
		back.appendChild(qrcodeEl);
		imageEl.appendChild(back);

		wrapperEl.appendChild(imageEl);

		if(this.config.showRaw) {
			const textEl = document.createElement("div");
			textEl.classList.add('qrcode__text');
			textEl.innerHTML = this.config.text;
			wrapperEl.appendChild(textEl);
		}

		return wrapperEl;
	}
});
